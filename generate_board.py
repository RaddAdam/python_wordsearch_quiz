class BoardGenerator():
    """
    Class used to create wordsearch board based on supplied text list
    """
    def __init__(self, text_list='./data/words.txt'):
        """
        input:
            @text_list (string): path to file containing valid words
        """
        self.text_file = text_list

    def randomize_row(self):
        """
        Take valid word and produce random arrangement of letters around a valid word
            return: randomized_row(list)
        """
        # TODO: Write method for random word table

    def create_horizontal(self):
        """
        Take input of randomized_row and produce a horizontal table entry
        """
        # TODO: Write horizontal method

    def create_vertical(self):
        """
        Take input of randomized_row and produce a vertical table entry
        """
        # TODO: Write vertical method

    def create_diagonal(self):
        """
        Take input of randomized_row and produce a diagonal table entry
        """
        # TODO: Write diagonal method

    def rows(self):
        """
        Create rows based on words
        """
        # open file
        valid_words_list = []
        with open(self.text_file, 'r') as tf:
            for count, line in enumerate(tf):
                # print("Line%i %s" % (count, line))
                valid_words_list.append(line)
        return valid_words_list

def main():
    board1 = BoardGenerator()
    board1.rows()
    
if __name__ == "__main__":
    main()