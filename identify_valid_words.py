class IdentifyWords():
    """
    Identify all valid words (contained in the attached word list) in the board.
    """
    def __init__(self, word_list='./data/words.txt'):
        """
        Need to identify valid words based on supplied word_list
            input:
                word_list (string): file path to word list
            return:
                valid_words (list): list of strings for valid words
        """
        self.word_list = word_list
    
    def validate_diagonal(self, word_table):
        """
        Taking a word_table object as input, check for valid diagonal entry
        """
    
    def validate_horizontal(self, word_table):
        """
        Taking a word_table object as input, check for valid horizontal entry
        """